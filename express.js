function express() {
  const funcArr = []
  const app = function (req, res) {
    let i = 0
    let nowF = funcArr[i]

    function next() {
      let nowF = funcArr[++i]
      if (nowF)
        nowF(req, res, next)
    }
    nowF(req, res, next)
  }
  app.use = function (f) {

    funcArr.push(f)

  }
  return app
}
let app = express()

// test
app.use(async (req, res, next) => {
  let data = await new Promise((s, f) => {
    setTimeout(() => {
      s(1)
    }, 5000);
  })
  console.log(data);
  next()
  console.log(4);

})
app.use((req, res, next) => {
  console.log(2);
  next()
  console.log(3);

})

app()
