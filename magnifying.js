import store from './../store'
import {
  debounce
} from './throttleAndDebounce.js'

let options = {
  baseFz: 20,
  unitPrecision: 6
}

const changeHTMLFontSize = function () {
  let width = document.documentElement.clientWidth
  console.log(`1REM 以 1920 屏幕的 ${options.baseFz} px, 现在宽度为 ${width}`)
  document.documentElement.style.fontSize = (options.baseFz * width / 1920).toFixed(options.unitPrecision) + 'px'
  let fz = parseFloat(document.documentElement.style.fontSize)
  if (store) {
    store.commit('resolutionChanged', {
      fz
    })
  }
}

const debouncechangeHTMLFZ = debounce(changeHTMLFontSize, 500)
const magnify = function (bfz, unitPrecision) {
  options.baseFz = bfz
  options.unitPrecision = unitPrecision
}

// 引起分辨率变化
window.addEventListener('resize', debouncechangeHTMLFZ)
window.addEventListener('load', changeHTMLFontSize)

export default magnify
