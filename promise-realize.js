class Promise2 {
  constructor(func) {
    let self = this
    this.data = undefined
    this._statusValue = 'pending'

    this.resolveFunc = undefined
    this.rejectFunc = undefined
    this.nextPromise = undefined
    Object.defineProperty(self, 'status', {
      enumerable: false,
      get() {
        return this._statusValue
      },
      set(value) {
        // 做一个锁，确定自己状态避免二次赋值
        if (value !== 'pending' && this._statusValue === 'pending') {
          if (value === 'resolved') {
            this._statusValue = 'resolved'
            this.statesDeterminedShouldDo('resolve')
            return
          } else if (value === 'rejected') {
            this._statusValue = 'rejected'
            this.statesDeterminedShouldDo('reject')
            return
          }
        }
      }
    })

    if (typeof func === 'function') {
      func(this.resolveF, this.rejectF)
    } else {
      console.error('Incorrect use in promise2 func must be a function')
      throw new Error('Incorrect use in promise2 func must be a function')
    }

  }
  get resolveF() {
    let self = this
    // 获得当前 Promise 的 resolved 后需要执行的函数，并设置 data
    return function (params) {
      if (self.status !== 'pending') {
        return
      }
      self.status = 'resolved'
      self.data = params
    }
  }
  get rejectF() {
    let self = this
    return function (params) {
      if (self.status !== 'pending') {
        return
      }
      self.status = 'rejected'
      self.data = params
    }
  }
  statesDeterminedShouldDo(stateStr) {
    // this.timeout = 异步执行 状态确定后需要执行的函数
    setTimeout(() => {
      this.statesDeterminedShouldDoSync(stateStr)
    }, 40)
  }
  statesDeterminedShouldDoSync(stateStr) {
    if (!this[stateStr + 'Func'] && !this.nextPromise) {
      // 失败的 promise 而且没有 then 和 catch
      if (stateStr == 'reject') console.error('Uncaught (in promise2)', this.data)
      return
    }
    if (!this[stateStr + 'Func'] && this.nextPromise) {
      // 将当前 promise 的 data 传给下一个，因为没有处理该 state 的函数
      this.nextPromise[stateStr + 'F'](this.data)
      this.nextPromise = undefined
      // 将当前 promise 的 nextPromise 指针设置为空，主动进行垃圾回收
      return
    }
    // 具有处理该 state 的函数。
    // 获取处理函数 this[stateStr + 'Func'] 即为 this.resolveF 或者 this.rejectF
    let result = this[stateStr + 'Func'](this.data)
    if (result instanceof Promise2 || result instanceof Promise) {
      result.then((d) => {
        this.nextPromise.resolveF(d)
        this.nextPromise = undefined
      }, (d) => {
        this.nextPromise.rejectF(d)
        this.nextPromise = undefined
      })
    } else {
      // 将结果交给下一个 promise
      this.nextPromise.resolveF(result)
      this.nextPromise = undefined
      // 将当前 promise 的 nextPromise 指针设置为空，主动进行垃圾回收
      return
    }
  }
  then(f31, f32) {
    if (f31 == undefined && f32 == undefined) {
      return this
    }
    let nextPromise = new Promise2()
    this.resolveFunc = f31
    this.rejectFunc = f32
    this.nextPromise = nextPromise
    // if (this.status == 'pending') {
    //   return nextPromise
    // }
    // 无论 当前 promise 是什么状态，都返回下一个进行收集。（同步收集函数，异步执行函数）
    return nextPromise
  }
  catch (f32) {
    return this.then(null, f32)
  }
  static resolve(data) {
    // 返回一个立即 resolve 的 promise
    let result = new Promise2((resolve) => {
      resolve(data)
    })
    return result
  }
  static reject(data) {
    let result = new Promise2((resolve, reject) => {
      reject(data)
    })
    return result
  }
}
