import localforage from 'localforage'

const store = localforage.createInstance({
  // driver      : localforage.WEBSQL,
  // Force WebSQL; same as using setDriver()
  name: 'myApp',
  // version: 1.0,
  // size        : 4980736,
  // Size of database, in bytes. WebSQL-only for now.
  storeName: 'key_value_pairs',
  // Should be alphanumeric, with underscores.
  description: 'myApp_store'
})

store.getItemByArray = (arr)=>{
  return store.getItem(arr.join('_'))
}

store.setItemByArray = (arr, data)=>{
  return store.setItem(arr.join('_'), data)
}

store.removeItemByArray = (arr) => {
  return store.removeItem(arr.join('_'))
}

console.log('====================================')
console.log('store build succed')
console.log('====================================')

export default store
