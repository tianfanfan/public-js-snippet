import axios from 'axios'
import Qs from 'qs';
axios.defaults.paramsSerializer = function (params) {
  return Qs.stringify(params, {
    arrayFormat: 'repeat'
  })
}

// axios.defaults.baseURL = '/api/v1/'
// axios.defaults.headers.get['Content-Type'] = 'application/json';
// axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.withCredentials = true
// 设置超时时间
axios.defaults.timeout = 10000;
// Add a request interceptor
// 请求发送成功 和 错误 中间件
axios.interceptors.request.use(function (config) {
  return config;
}, function (error) {
  return Promise.reject(error);
});

// 响应成功 和 错误中间件 超时直接结束
// axios.interceptors.response.use(function (response) {
//   return response;
// }, function (error) {
//   return Promise.reject(error);
// });

// 在 main.js 设置全局的请求次数，请求的间隙

// 错误 重新尝试 1 次
let time = process.env ? process.env.NUM : 1
axios.defaults.retry = time;
// 设置重发请求的间隔时间
axios.defaults.retryDelay = 2000;

axios.interceptors.response.use(undefined, function axiosRetryInterceptor(err) {
  console.error('====================================')
  console.error(err)
  console.error('====================================')
  let config = err.config;
  // If config does not exist or the retry option is not set, reject
  if (!config || !config.retry) return Promise.reject(err);
  // Set the variable for keeping track of the retry count
  config.__retryCount = config.__retryCount || 0;
  // Check if we've maxed out the total number of retries
  if (config.__retryCount >= config.retry) {
    // Reject with the error
    return Promise.reject(err);
  }

  // Increase the retry count
  config.__retryCount += 1;

  // Create new promise to handle exponential backoff
  let backoff = new Promise(function (resolve) {
    setTimeout(function () {
      resolve();
    }, config.retryDelay || 1);
  });

  // Return the promise in which recalls axios to retry the request
  return backoff.then(function () {
    // axios 如果配置了 baseUrl , config 里面也会有，会产生多次拼接。
    // 所以 此处把 baseUrl 清空，因为已经拼接过了
    return axios({ ...config,
      baseURL: ''
    });
  });
});

export default axios
